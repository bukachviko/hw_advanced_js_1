'use strict'

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get employeeName() {
        return this.name;
    }

    set employeeName(value) {
        this.name = value;
    }

   get employeeAge() {
        return this.age;
    }

    set employeeAge(age) {
        return this.age;
    }

    get employeeSalary() {
        return this.salary;
    }

    set employeeSalary(number) {
        return this.salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get employeeSalary() {
        return this.salary * 3;
    }
    set employeeSalary(progSalary) {
        this.salary = progSalary;
    }
}

// const employee1 = new Employee('Victoria', 30, 1000);
// const employee2 = new Employee('Maks', 34, 2000);
// const employee3 = new Employee('Eva', 18, 3000);

const programmer1 = new Programmer ("Maxim", 34, 4000, "English");
const programmer2 = new Programmer ("Den", 28, 3000, "German");
const programmer3 = new Programmer ("Katya", 30, 2500, "Polish");

console.log(programmer1.employeeSalary);
console.log(programmer2.employeeSalary);
console.log(programmer3.employeeSalary);
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);


// 1) Об'єкти у JS  мають власні властивості та посилання на об'єкт прототип. При спробі звертання до властивості об'єкта,
// ця властивість буде шукатися не лише на самому об'єкті, а й на його прототипі, прототипі його прототипа, і так далі, поки або не буде знайдена властивість з відповідною назвою, або буде досягнутий кінець ланцюжка прототипів.
// 2) Метод Super() викликається класом-нащадком коли є конструктор у класі-батька щоб він наслідував його властивості та методи.